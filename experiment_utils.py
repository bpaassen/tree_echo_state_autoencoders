"""
Contains utility functions for the auto-encoding experiments.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import numpy as np
from edist import ted


def tree_rmse(model, trees):
    """ Computes the root mean square error in auto-encoding the given trees
    with the given model, using the tree edit distance (Zhang and Shasha, 1989)
    as measure of distance.

    Args:
    model: A tree echo state auto-encoder (refer to
           tesae.tree_echo_state_auto_encoder.TESAutoEncoder).
    trees: A list of trees in node list/adjacency list format.

    Returns:
    rmse:  The root mean square error.
    """
    err = 0
    for tree in trees:
        # auto-encode the tree
        _, h           = model.encode(*tree)
        nodes, adj, *_ = model.decode(h, max_size = 2 * len(tree[0]) + 5)
        # if the auto-encoding was precise, we have no error
        if nodes == tree[0] and adj == tree[1]:
            continue
        # compute the squared tree edit distance between
        # the original tree and the auto-encoded version
        d = ted.standard_ted(nodes, adj, *tree)
        err += d ** 2
    # compute the RMSE
    return np.sqrt(err / len(trees))
