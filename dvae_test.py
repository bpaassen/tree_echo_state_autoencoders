"""
Tests the tree-to-igraph conversion and vice versa of dvae.py.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import unittest
import edist.tree_utils as tree_utils
import tesae.tree_grammar as tree_grammar
import numpy as np
import tesae.tree as tree
import dvae
import igraph

# set up grammar
import tesae.tree_grammar as tree_grammar
alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
nonts = ['S']
start = 'S'
rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

# set up a function to generate a tree
def sample_tree(max_ops = 3):
    remaining_ops = max_ops
    root = tree.Tree('$')
    stk = [root]
    while stk:
        par = stk.pop()
        if remaining_ops > 0:
            if par._label != 'not':
                r = np.random.choice(5, 1, p = [0.3, 0.3, 0.1, 0.15, 0.15])
            else:
                r = np.random.choice(5, 1, p = [0.3, 0.3, 0.0, 0.2, 0.2])
        else:
            if par._label != 'not':
                r = np.random.choice(5, 1, p = [0., 0., 0.2, 0.4, 0.4])
            else:
                r = np.random.choice(5, 1, p = [0., 0., 0., 0.5, 0.5])
        if r == 0:
            child = tree.Tree('and')
            stk.append(child)
            stk.append(child)
            remaining_ops -= 1
        elif r == 1:
            child = tree.Tree('or')
            stk.append(child)
            stk.append(child)
            remaining_ops -= 1
        elif r == 2:
            child = tree.Tree('not')
            stk.append(child)
        elif r == 3:
            child = tree.Tree('x')
        elif r == 4:
            child = tree.Tree('y')
        else:
            raise ValueError('internal error!')
        par._children.append(child)
    return root._children[0].to_list_format()

types = []
type_idxs = {}
num_types = 0
for sym in alphabet:
    types.append(sym)
    type_idxs[sym] = num_types
    num_types += 1

class TestGraphConversion(unittest.TestCase):

    def test_tree_to_igraph(self):
        # consider a few example trees and ensure that the resulting
        # graph has exactly the expected form
        nodes = ['x']
        adj   = [[]]
        g, n  = dvae.tree_to_igraph(nodes, adj, type_idxs)
        expected_adj = [[1], [2], []]
        self.assertEqual(expected_adj, g.get_adjlist())
        expected_types = [0, type_idxs['x'] + 2, 1]
        self.assertEqual(expected_types, g.vs['type'])

        nodes = ['not', 'x']
        adj   = [[1], []]
        g, n  = dvae.tree_to_igraph(nodes, adj, type_idxs)
        expected_adj = [[1], [2], [3], []]
        self.assertEqual(expected_adj, g.get_adjlist())
        expected_types = [0, type_idxs['not'] + 2, type_idxs['x'] + 2, 1]
        self.assertEqual(expected_types, g.vs['type'])

        nodes = ['or', 'x', 'y']
        adj   = [[1, 2], [], []]
        g, n  = dvae.tree_to_igraph(nodes, adj, type_idxs)
        expected_adj = [[1], [2, 3], [4], [4], []]
        self.assertEqual(expected_adj, g.get_adjlist())
        expected_types = [0, type_idxs['or'] + 2, type_idxs['x'] + 2, type_idxs['y'] + 2, 1]
        self.assertEqual(expected_types, g.vs['type'])

    def test_igraph_to_tree(self):
        # consider a few example graphs and ensure that the resulting
        # tree has exactly the expected form
        g = igraph.Graph(directed = True)
        g.add_vertices(3)
        g.add_edge(0, 1)
        g.add_edge(1, 2)
        g.vs['type'] = [0, type_idxs['x'] + 2, 1]

        expected_nodes = ['x']
        expected_adj   = [[]]
        nodes, adj  = dvae.igraph_to_tree(g, types)
        self.assertEqual(expected_adj, adj)
        self.assertEqual(expected_nodes, nodes)

        g = igraph.Graph(directed = True)
        g.add_vertices(4)
        g.add_edge(0, 1)
        g.add_edge(1, 2)
        g.add_edge(2, 3)
        g.vs['type'] = [0, type_idxs['not'] + 2, type_idxs['x'] + 2, 1]

        expected_nodes = ['not', 'x']
        expected_adj   = [[1], []]
        nodes, adj  = dvae.igraph_to_tree(g, types)
        self.assertEqual(expected_adj, adj)
        self.assertEqual(expected_nodes, nodes)

        g = igraph.Graph(directed = True)
        g.add_vertices(5)
        g.add_edge(0, 1)
        g.add_edge(1, 2)
        g.add_edge(1, 3)
        g.add_edge(2, 4)
        g.add_edge(3, 4)
        g.vs['type'] = [0, type_idxs['or'] + 2, type_idxs['x'] + 2, type_idxs['y'] + 2, 1]

        expected_nodes = ['or', 'x', 'y']
        expected_adj   = [[1, 2], [], []]
        nodes, adj  = dvae.igraph_to_tree(g, types)
        self.assertEqual(expected_adj, adj)
        self.assertEqual(expected_nodes, nodes)

    def test_large_scale_graph_conversion(self):
        # sample a few hundred boolean trees and ensure that
        # the to-graph and from-graph conversion end up constructing the
        # same tree
        for _ in range(300):
            nodes, adj = sample_tree()
            g, n = dvae.tree_to_igraph(nodes, adj, type_idxs)
            nodes_actual, adj_actual = dvae.igraph_to_tree(g, types)
            self.assertEqual(nodes, nodes_actual)
            self.assertEqual(adj, adj_actual)

if __name__ == '__main__':
    unittest.main()
