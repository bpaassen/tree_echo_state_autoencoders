"""
Implements a recursive tree data structure for internal use.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

class Tree:
    """ Models a tree as a recursive data structure with a label and
    children.

    Attributes:
    _label: The label of this tree.
    _children: The children of this tree. Should be a list of trees.
    """
    def __init__(self, label, children = None):
        self._label = label
        if(children is None):
            self._children = []
        else:
            self._children = children

    def __str__(self):
        out = str(self._label)
        if(not self._children):
            return out
        else:
            child_strs = [str(child) for child in self._children]
            return out + '(%s)' % ', '.join(child_strs)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if isinstance(other, Tree):
            return self._label == other._label and self._children == other._children
        return False

    def to_list_format(self):
        """ Convers this tree to node list/adjacency list format
        via depth first search.

        Args: None

        Returns:
        nodes: the node list.
        adj:   the adjacency list.
        """
        # initialize node and adjacency list
        nodes = []
        adj   = []
        # perform the depth first search via a stack which stores
        # the parent index and the current tree
        stk = [(-1, self)]
        while(stk):
            p, tree = stk.pop()
            i = len(nodes)
            # append the label to the node list
            nodes.append(tree._label)
            # append a new empty child list
            adj.append([])
            # append the current node to its parent
            if(p >= 0):
                adj[p].append(i)
            # push the children onto the stack
            for c in range(len(tree._children)-1, -1, -1):
                if(tree._children[c] is None):
                    continue
                if(isinstance(tree._children[c], list)):
                    for c2 in range(len(tree._children[c]) -1, -1, -1):
                        stk.append((i, tree._children[c][c2]))
                    continue
                stk.append((i, tree._children[c]))
        return nodes, adj
