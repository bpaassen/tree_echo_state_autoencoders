"""
Implements echo state auto-encoders, consisting of a simple recurrent
neural net for encoding and decoding.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import numpy as np
import sklearn.svm
import tesae.crj as crj

class ESAutoEncoder:
    """ A class that implements an echo state network which can encode an
    input time series as a vector and can decode it back into a time series,
    following the sequence-to-sequence learning scheme of Sutskever et al. (2014).

    For the encoding, we treat the final state of the reservoir after reading
    the input time series as representation of the time series. For the
    decoding, we use the encoding as initial state of the reservoir and then
    produce an output vector via a learned output mapping. This output vector
    is fed back into the network as input. This process continues until a
    learned classifier determines that the output sequence should end.

    Note that we only learn the output mapping during the decoding phase and
    the classifier. Both can be learned using a teacher forcing approach.

    To initialize the reservoir we use a cycle reservoir with jumps
    (Tino and Rodan, 2012).

    Attributes:
    _dim:      The encoding dimensionality, i.e. the number of neurons
               in the reservoir.
    _v:        The absolute value of input weights. Defaults to 0.1.
    _w_c:      The weight on cycle connections. Must be in range [0, 1).
               Defaults to 0.8.
    _w_j:      The weight on jump connections. Must be in range [0, 1).
               Defaults to 0.3.
    _l:        The length of jumps. Defaults to _dim / 10.
    _regul:    The L2 regularization strength. Defaults to 1E-5.
    _one_hot_correction: Set to true if, during decoding, output
               vectors should be corrected to one-hot vectors. Defaults to
               False.
    W_:        The reservoir matrix.
    U_:        The input to reservoir connections.
    V_:        The learned output decoding weights.
    controller_: The learned classifier to determine when the sequence should
               end.

    """
    def __init__(self, dim, v = 0.1, w_c = 0.9, w_j = 0.3, l = None, regul = 1E-5, one_hot_correction = False):
        self._dim = dim
        self._v   = v
        self._w_c = w_c
        self._w_j = w_j
        if l is None:
            self._l = max(2, int(self._dim / 10))
        else:
            self._l = l
        self._regul = regul
        self._one_hot_correction = one_hot_correction

    def encode(self, X):
        """ Encodes an input sequence as a vector.

        Args:
        X: A sequence, given as a T x n matrix where T is the
           sequence length and n is the dimensionality.

        Returns:
        h:   The vectorial encoding of the input time series.
        """
        T, n = X.shape
        # compute first state
        h = np.tanh(np.dot(self.U_, X[0, :]))
        # compute remaining states
        for t in range(1, T):
            # note that we use * for the multiplication with _W because it
            # is a sparse matrix
            h = np.tanh(np.dot(self.U_, X[t, :]) + self.W_ * h)
        return h

    def decode(self, h, max_len = None):
        """ Decodes the given vector back into a sequence.

        Args:
        h: A self._dim dimensional vector.
        max_len: (optional) a maximum length for the generated sequence.
                 Default to none.

        Returns:
        Y: A T x n-dimensional sequence decoded from the given vector.
        """
        # initialize output
        Y = []
        # process decoding
        # classify the current reservoir state and check if we should stop
        control_signal = self.controller_.predict(np.expand_dims(h, 0))[0]
        while control_signal == 0 and (max_len is None or len(Y) < max_len):
            # otherwise, generate a new output vector
            y = np.dot(self.V_, h)
            Y.append(y)
            # transform it into a one-hot vector
            if self._one_hot_correction:
                j = np.argmax(y)
                y = np.zeros(len(y))
                y[j] = 1.
            # compute the new reservoir state
            h = np.tanh(np.dot(self.U_, y) + self.W_ * h)
            # and check if we should stop
            control_signal = self.controller_.predict(np.expand_dims(h, 0))[0]
        # transform Y into a numpy array
        if len(Y) > 0:
            Y = np.stack(Y)
        else:
            Y = np.array([])
        # and return it
        return Y

    def _decode_forced(self, h, X):
        """ Decodes the given sequence from the given vector via teacher
        forcing.

        Args:
        h: A self._dim dimensional vector.
        X: The ground-truth sequence that should be decoded.

        Returns:
        H: A T+1 x self._dim dimensional sequence containing the reservoir
           states during decoding.
        """
        T, n = X.shape
        # initialize output matrix
        H = np.zeros((T+1, self._dim))
        # set initial state to encoding
        H[0, :] = h
        # then generate all subsequent reservoir states
        # compute remaining states
        for t in range(T):
            # note that we use * for the multiplication with _W because it
            # is a sparse matrix
            H[t+1, :] = np.tanh(np.dot(self.U_, X[t, :]) + self.W_ * H[t, :])
        return H


    def fit(self, seqs):
        """ Fits this auto-encoder to the given set of sequences.

        Args:
        seqs: A list of sequences, each given as a T x n matrix where T is the
              sequence length and n is the dimensionality.

        Returns: this model after training
        """
        n = seqs[0].shape[1]

        # set up reservoir
        self.U_ = crj.setup_input_weight_matrix(n, self._dim, self._v)
        self.W_ = crj.setup_reservoir_matrix(self._dim, self._w_c, self._w_j, self._l)

        # encode all input sequences and force-decode them afterwards.
        # This, then, constitutes our training data
        H_out = []
        Y_out = []
        H_control = []
        Y_control = []
        for seq in seqs:
            h = self.encode(seq)
            H = self._decode_forced(h, seq)
            H_out.append(H[:len(seq), :])
            Y_out.append(seq)
            H_control.append(H)
            y_control = np.zeros(len(H))
            y_control[-1] = 1.
            Y_control.append(y_control)
        # concatenate the training data
        H_out = np.concatenate(H_out, axis=0)
        Y_out = np.concatenate(Y_out, axis=0)
        H_control = np.concatenate(H_control, axis=0)
        Y_control = np.concatenate(Y_control, axis=0)

        # train the output mapping via linear regression
        C_inv = np.dot(H_out.T, H_out)
        C_inv = np.linalg.inv(C_inv + self._regul * np.eye(self._dim))
        C_out = np.dot(Y_out.T, H_out)
        self.V_ = np.dot(C_out, C_inv)

        # train the controller as an SVM
        self.controller_ = sklearn.svm.SVC( C = 1. / self._regul , class_weight = 'balanced')
        self.controller_.fit(H_control, Y_control)

        return self


def one_hot_coding(seq, n = None):
    """ Preprocesses a discrete input sequence to become one-hot-coded.
    For example, the sequence [1, 0, 3] becomes

    [[0., 1., 0., 0.], [1., 0., 0., 0.], [0., 0., 0., 1.]]

    In other words, we generate an output array of dimensionality
    len(seq) x max(seq)+1.

    Args:
    seq: A sequence of integers in the range [0, n].
    n:   (optional) a dimensionality of the one-hot-coding. Defualts
         to max(seq)+1.

    Return:
    X: A numpy array of shape len(seq) x n where X[t, i] is 1. if
       seq[t] == i and 0. otherwise.
    """
    if n is None:
        n = np.max(seq)+1

    X = np.zeros((len(seq), n))
    for t in range(len(seq)):
        X[t, seq[t]] = 1.
    return X

import tesae.tree as tree

def produce(grammar, X):
    """ Produces a tree using the given tree grammar and the given
    sequence of activations. In each time step, we select the
    rule with the highest activation.

    Args:
    grammar: A regular tree grammar.
    X: A T x n sequence of activations.

    Returns:
    seq:   The rule sequence produces.
    nodes: The node list of the produces tree.
    adj:   The adjacency list of the produced tree.
    valid: True if the input sequence lead to a valid tree; false
           otherwise.
    """
    root = tree.Tree('$', [tree.Tree(grammar._start)])
    stk = [(root, 0)]
    seq = []
    for t in range(len(X)):
        if not stk:
            # if there is no nonterminal symbol left, we have to stop
            # the generation process
            nodes, adj = root._children[0].to_list_format()
            return seq, nodes, adj, False
        # pop the current parent and child index
        par, c = stk.pop()
        # retrieve the current nonterminal
        A = par._children[c]._label
        # if the nonterminal ends with a *, we have to handle a list
        if(isinstance(A, str) and A.endswith('*')):
            lst_node = par._children[c]
            # here, two rules are possible:
            if X[t, 0] > X[t, 1]:
                # the zeroth rule completes the list and means
                # that we replace the entire tree node with the
                # child list
                par._children[c] = par._children[c]._children
                seq.append(0)
                continue
            else:
                # the first rule continues the list, which means
                # that we put the current nonterminal on the stack
                # again
                stk.append((par, c))
                # and we create another nonterminal child and put
                # it on the stack
                lst_node._children.append(tree.Tree(A[:-1]))
                stk.append((lst_node, len(lst_node._children) - 1))
                seq.append(1)
                continue
        # if the nonterminal ands with a ?, we have to handle an optional
        # node
        if(isinstance(A, str) and A.endswith('?')):
            # here, two rules are possible
            if X[t, 0] > X[t, 1]:
                # the zeroth rule means we replace the nonterminal with
                # None
                par._children[c] = None
                seq.append(0)
                continue
            else:
                # the first rule means we replace the nonterminal
                # with its non-optional version and push it on the
                # stack again
                par._children[c]._label = A[:-1]
                stk.append((par, c))
                seq.append(1)
                continue
        # select the production rule with maximum activation
        r = np.argmax(X[t, :len(grammar._rules[A])])
        seq.append(r)
        sym, rights = grammar._rules[A][r]
        # replace A in the input tree with sym(rights)
        subtree = par._children[c]
        subtree._label = sym
        # push all new child nonterminals onto the stack and
        # to the tree
        for c in range(len(rights)-1, -1, -1):
            subtree._children.insert(0, tree.Tree(rights[c]))
            stk.append((subtree, c))
    # set validity to false if the stack is not empty yet
    valid = len(stk) == 0
    # return the final tree in node list adjacency list format
    nodes, adj = root._children[0].to_list_format()
    return seq, nodes, adj, valid

from tesae.tree_grammar import TreeParser

class ESTreeWrapper:
    """ This is a wrapper for the ESAutoEncoder that provides the same
    interface as the tree echo state auto encoder.

    Attributes:
    _grammar: A tree grammar.
    _parser:  A parser for this tree grammar.
    _ae:      An ESAutoEncoder instance.
    """
    def __init__(self, grammar, dim, v = 0.1, w_c = 0.9, w_j = 0.3, l = None, regul = 1E-5):
        self._grammar = grammar
        self._parser  = TreeParser(grammar)
        self._ae      = ESAutoEncoder(dim, v, w_c, w_j, l, regul, one_hot_correction = True)

    def encode(self, nodes, adj):
        """ Encodes the given tree as a vector and parses it at the same time.

        Args:
        nodes: a node list for the input tree.
        adj:   an adjacency list for the input tree.

        Returns:
        seq:   a rule sequence such that self._grammar.produce(seq) is
               equal to nodes, adj.
        h:     A self._dim dimensional vectorial encoding of the input
               tree.

        Throws: ValueError if the input is not a tree or not part of the
                language.
        """
        seq = self._parser.parse(nodes, adj)
        h   = self._ae.encode(one_hot_coding(seq, self._ae.U_.shape[1]))
        return seq, h

    def decode(self, h, max_size = None):
        """ Decodes the given vector back into a tree. This is only
        available if fit has been called.

        Args:
        h: A self._dim dimensional encoding of the tree that should be decoded.
        max_size: A maximum tree size to prevent infinite recursion. Defaults
                  to None.

        Returns:
        nodes: the node list of the produced tree.
        adj:   the adjacency list of the produced tree.
        seq:   the rule sequence generating the produced tree.
        valid: true if the produced tree is valid and false otherwise.
        """
        X = self._ae.decode(h, max_len = max_size)
        seq, nodes, adj, valid = produce(self._grammar, X)
        return nodes, adj, seq, valid

    def fit(self, trees):
        """ Trains this auto-encoder on the given tree dataset.

        Args:
        trees: A list of trees, each specified by a node list
               and an adjacency list.

        Throws: ValueError if the input is not a tree or not part of the
                language.
        """
        # determine the maximum number of rules for the one-hot coding
        n = 0
        for left in self._grammar._rules:
            if len(self._grammar._rules[left]) > n:
                n = len(self._grammar._rules[left])
        n += 1
        # encode all trees via one-hot coding
        seqs = []
        for i in range(len(trees)):
            seq = self._parser.parse(*trees[i])
            seqs.append(one_hot_coding(seq, n))
        self._ae.fit(seqs)
        return self
