"""
Implements tests for the tree implementation.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import unittest
import tesae.tree as tree

class TestTree(unittest.TestCase):

    def test_to_list_format(self):
        # set up a simple tree
        T = tree.Tree('a', [tree.Tree('b', [tree.Tree('c'), tree.Tree('d')]), tree.Tree('e')])
        # check the tree
        self.assertEqual('a(b(c, d), e)', str(T))
        # check the list conversion
        nodes_expected = ['a', 'b', 'c', 'd', 'e']
        adj_expected   = [[1, 4], [2, 3], [], [], []]
        nodes_actual, adj_actual = T.to_list_format()
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)


if __name__ == '__main__':
    unittest.main()
