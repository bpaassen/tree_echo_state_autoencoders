"""
Implements tests for tree echo state auto encoders.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import unittest
import numpy as np
import edist.tree_utils as tree_utils
import edist.ted as ted
import tesae.tree as tree
import tesae.tree_grammar as tree_grammar
import tesae.tree_echo_state_auto_encoder as ae

from sklearn.kernel_ridge import KernelRidge

class TestTESParser(unittest.TestCase):

    def test_initialize_reservoir(self):
        dim = 8
        sparsity = 0.26
        radius = 0.9

        W = ae._initialize_reservoir(dim, sparsity, radius)

        # check dimensions
        self.assertEqual(dim, W.shape[0])
        self.assertEqual(dim, W.shape[1])

        # check sparsity
        expected_nonzeros = dim * int((sparsity * dim))
        actual_nonzeros = np.sum(np.abs(W) > 1E-8)
        self.assertEqual(expected_nonzeros, actual_nonzeros, str(W))

        # check radius
        rho = np.max(np.abs(np.linalg.eigvals(W)))
        self.assertAlmostEqual(radius, rho)

    def test_parse(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # set up the TESParser
        parser = ae.TESParser(grammar, 5)
        # test a few trees which should be valid

        nodes = ['not', 'x']
        adj   = [[1], []]
        expected_seq = [2, 3]
        expected_x = np.tanh(parser._rules['x'][0]._b)
        expected_notx = np.tanh(np.dot(parser._rules['not'][0]._Ws[0], expected_x) + parser._rules['not'][0]._b)
        actual_seq, actual_h = parser.parse(nodes, adj)
        self.assertEqual(expected_seq, actual_seq)
        np.testing.assert_array_almost_equal(expected_notx, actual_h)

        nodes = ['and', 'x', 'y']
        adj   = [[1, 2], [], []]
        expected_seq = [0, 3, 4]
        expected_y = np.tanh(parser._rules['y'][0]._b)
        expected_h = np.tanh(np.dot(parser._rules['and'][0]._Ws[0], expected_x) + np.dot(parser._rules['and'][0]._Ws[1], expected_y) + parser._rules['and'][0]._b)
        actual_seq, actual_h = parser.parse(nodes, adj)
        self.assertEqual(expected_seq, actual_seq)
        np.testing.assert_array_almost_equal(expected_h, actual_h)

        nodes = ['and', 'x', 'not', 'x']
        adj   = [[1, 2], [], [3], []]
        expected_seq = [0, 3, 2, 3]
        expected_h = np.tanh(np.dot(parser._rules['and'][0]._Ws[0], expected_x) + np.dot(parser._rules['and'][0]._Ws[1], expected_notx) + parser._rules['and'][0]._b)
        actual_seq, actual_h = parser.parse(nodes, adj)
        self.assertEqual(expected_seq, actual_seq)
        np.testing.assert_array_almost_equal(expected_h, actual_h)

    def test_parse2(self):
        # set up a tree grammar for Boolean formulae in CNF
        alphabet = {'and' : 1, 'or' : 1, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['F', 'C', 'L']
        start = 'F'
        rules = { 'F' : [('and', ['C*'])], 'C' : [('or', ['L*'])], 'L' : [('not', ['L']), ('x', []), ('y', [])] }
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # set up parser
        parser = ae.TESParser(grammar, 5)

        # set up an example tree and check whether it is parsed correctly
        expected_nodes = ['and', 'or', 'x', 'or', 'not', 'x']
        expected_adj   = [[1, 3], [2], [], [4], [5], []]
        expected_seq   = [0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0]

        expected_x    = np.tanh(parser._rules['x'][0]._b)
        expected_notx = np.tanh(np.dot(parser._rules['not'][0]._Ws[0], expected_x) + parser._rules['not'][0]._b)
        expected_orx  = np.tanh(np.dot(parser._rules['or'][0]._Ws[0], expected_x) + parser._rules['or'][0]._b)
        expected_ornotx  = np.tanh(np.dot(parser._rules['or'][0]._Ws[0], expected_notx) + parser._rules['or'][0]._b)
        expected_h    = np.tanh(np.dot(parser._rules['and'][0]._Ws[0], expected_orx + expected_ornotx)+ parser._rules['and'][0]._b)

        actual_seq, actual_h = parser.parse(expected_nodes, expected_adj)
        self.assertEqual(expected_seq, actual_seq)
        actual_nodes, actual_adj = grammar.produce(actual_seq)
        self.assertEqual(expected_nodes, actual_nodes)
        self.assertEqual(expected_adj, actual_adj)
        np.testing.assert_array_almost_equal(expected_h, actual_h)

class TestTESGrammar(unittest.TestCase):

    def test_decode(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # set up TESGrammar
        dim = 1
        tes_grammar = ae.TESGrammar(grammar, dim)
        # re-set the reservoir matrices for the 'and' rule manually
        tes_grammar._rules['S'][0]._Ws[0][:, :] = 1.
        tes_grammar._rules['S'][0]._bs[0][:] = -0.2
        tes_grammar._rules['S'][0]._Ws[1][:, :] = 0.
        tes_grammar._rules['S'][0]._bs[1][:] = 0.
        # set the classifier manually
        tes_grammar._Vs = {}
        tes_grammar._Vs['S'] = np.array([
            [1., 0.], # and rule
            [0.,-1.], # or rule
            [0.,-1.], # not rule
            [-1., 1E-3], # x rule
            [0.,-1.], # y rule
        ])

        # test-decode an encoding
        h = np.tanh([.6])

        expected_nodes = ['and', 'and', 'and', 'x', 'x', 'x', 'x']
        expected_adj = [[1, 6], [2, 5], [3, 4], [], [], [], []]
        expeted_seq = tree_grammar.TreeParser(grammar).parse(expected_nodes, expected_adj)

        nodes, adj, seq = tes_grammar.decode(h)

        self.assertEqual(expected_nodes, nodes)
        self.assertEqual(expected_adj, adj)
        self.assertEqual(expeted_seq, seq)

    def test_produce(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # set up TESGrammar
        dim = 5
        tes_grammar = ae.TESGrammar(grammar, dim)

        # produce a simple tree
        nodes_expected = ['and', 'x', 'not', 'x']
        adj_expected   = [[1, 2], [], [3], []]
        h = np.tanh(np.random.randn(dim))

        # set up expected decodings
        g = np.copy(h)
        expected_x = np.tanh(np.dot(tes_grammar._rules['S'][0]._Ws[0], g) + tes_grammar._rules['S'][0]._bs[0])
        g -= expected_x
        expected_notx = np.tanh(np.dot(tes_grammar._rules['S'][0]._Ws[1], g) + tes_grammar._rules['S'][0]._bs[1])

        expected_x2 = np.tanh(np.dot(tes_grammar._rules['S'][2]._Ws[0], expected_notx) + tes_grammar._rules['S'][2]._bs[0])

        hs_expected = [h, expected_x, expected_notx, expected_x2]

        nonts_expected = ['S', 'S', 'S', 'S']

        seq = [0, 3, 2, 3]
        nodes_actual, adj_actual, nonts, hs = tes_grammar.produce(h, seq)
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)
        self.assertEqual(nonts_expected, nonts)

        self.assertEqual(len(seq), len(hs))
        for r in range(len(hs)):
            np.testing.assert_array_almost_equal(hs_expected[r], hs[r], decimal = 3, err_msg = 'Error in step %d' % r)


    def test_produce2(self):
        # set up a tree grammar for Boolean formulae in CNF
        alphabet = {'and' : 1, 'or' : 1, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['F', 'C', 'L']
        start = 'F'
        rules = { 'F' : [('and', ['C*'])], 'C' : [('or', ['L*'])], 'L' : [('not', ['L']), ('x', []), ('y', [])] }
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

        # set up TESGrammar
        dim = 5
        tes_grammar = ae.TESGrammar(grammar, dim)

        # produce a simple tree
        nodes_expected = ['and', 'or', 'x', 'or', 'not', 'x']
        adj_expected   = [[1, 3], [2], [], [4], [5], []]
        h = np.tanh(np.random.randn(dim))

        # set up expected decodings
        expected_Cstar = np.tanh(np.dot(tes_grammar._rules['F'][0]._Ws[0], h) + tes_grammar._rules['F'][0]._bs[0])
        expected_orx = np.tanh(np.dot(tes_grammar._nont_star_Ws['C*'], expected_Cstar) + tes_grammar._nont_star_bs['C*'])
        expected_ornotx = np.tanh(np.dot(tes_grammar._nont_star_Ws['C*'], expected_Cstar - expected_orx) + tes_grammar._nont_star_bs['C*'])
        expected_Cstop = expected_Cstar - expected_orx - expected_ornotx

        expected_Lstar1 = np.tanh(np.dot(tes_grammar._rules['C'][0]._Ws[0], expected_orx) + tes_grammar._rules['C'][0]._bs[0])
        expected_x = np.tanh(np.dot(tes_grammar._nont_star_Ws['L*'], expected_Lstar1) + tes_grammar._nont_star_bs['L*'])
        expected_Lstop = expected_Lstar1 - expected_x

        expected_Lstar2 = np.tanh(np.dot(tes_grammar._rules['C'][0]._Ws[0], expected_ornotx) + tes_grammar._rules['C'][0]._bs[0])
        expected_notx = np.tanh(np.dot(tes_grammar._nont_star_Ws['L*'], expected_Lstar2) + tes_grammar._nont_star_bs['L*'])
        expected_Lstop2 = expected_Lstar2 - expected_notx

        expected_x2 = np.tanh(np.dot(tes_grammar._rules['L'][0]._Ws[0], expected_notx) + tes_grammar._rules['L'][0]._bs[0])

        hs_expected = [h, expected_Cstar, expected_orx, expected_Lstar1, expected_x, expected_Lstop, expected_Cstar - expected_orx, expected_ornotx, expected_Lstar2, expected_notx, expected_x2, expected_Lstop2, expected_Cstop]

        nonts_expected = ['F', 'C*', 'C', 'L*', 'L', 'L*', 'C*', 'C', 'L*', 'L', 'L', 'L*', 'C*']

        seq = [0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0]
        nodes_actual, adj_actual, nonts, hs = tes_grammar.produce(h, seq)
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)
        self.assertEqual(nonts_expected, nonts)

        self.assertEqual(len(seq), len(hs))
        for r in range(len(hs)):
            np.testing.assert_array_almost_equal(hs_expected[r], hs[r], decimal = 3, err_msg = 'Error in step %d (nonterminal %s)' % (r, nonts[r]))


    def test_fit(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # set up TESGrammar
        dim = 1
        tes_grammar = ae.TESGrammar(grammar, dim)
        # re-set the reservoir matrices for the 'and' rule manually
        tes_grammar._rules['S'][0]._Ws[0][:, :] = 1.
        tes_grammar._rules['S'][0]._bs[0][:] = -.15
        tes_grammar._rules['S'][0]._Ws[1][:, :] = 0.
        tes_grammar._rules['S'][0]._bs[1][:] = 0.

        # set up training data
        trees = [
            (['x'], [[]]),
            (['and', 'x', 'x'], [[1, 2], [], []]),
            (['and', 'and', 'x', 'x', 'x'], [[1, 4], [2, 3], [], [], []]),
            (['and', 'and', 'and', 'x', 'x', 'x', 'x'], [[1, 6], [2, 5], [3, 4], [], [], [], []])
        ]
        H = np.tanh(np.expand_dims(np.arange(len(trees), dtype=float), 1) * 0.2)

        # fit the TESGrammar to the data
        tes_grammar.fit_trees(H, trees)

        # check whether the resulting grammar indeed decodes the expected trees
        for i in range(len(trees)):
            nodes, adj, _ = tes_grammar.decode(H[i, :], max_size = 50)
            self.assertEqual(trees[i][0], nodes)
            self.assertEqual(trees[i][1], adj)

        # try the same thing again, but with high-dimensional random codings
        # this time
        dim = 64
        tes_grammar = ae.TESGrammar(grammar, dim)
        H = np.tanh(np.random.randn(len(trees), dim))

        # fit the TESGrammar to the data
        tes_grammar.fit_trees(H, trees)

        # check whether the resulting grammar indeed decodes the expected trees
        for i in range(len(trees)):
            nodes, adj, _ = tes_grammar.decode(H[i, :], max_size = 50)
            self.assertEqual(trees[i][0], nodes)
            self.assertEqual(trees[i][1], adj)


class TestTESAutoEncoder(unittest.TestCase):

    def test_fit(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

        # set up TES autoencoder
        dim = 64
        tes_ae = ae.TESAutoEncoder(grammar, dim)

        # set up training data
        trees = [
            (['x'], [[]]),
            (['and', 'x', 'x'], [[1, 2], [], []]),
            (['and', 'and', 'x', 'x', 'x'], [[1, 4], [2, 3], [], [], []]),
            (['and', 'and', 'and', 'x', 'x', 'x', 'x'], [[1, 6], [2, 5], [3, 4], [], [], [], []])
        ]

        # fit the auto-encoder to the data
        tes_ae.fit_svm(trees)

        # check whether the auto-encoding works
        for i in range(len(trees)):
            nodes, adj, _ = tes_ae.decode(tes_ae.encode(*trees[i])[1], max_size = 15)
            self.assertEqual(trees[i][0], nodes)
            self.assertEqual(trees[i][1], adj)

    def test_fit2(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

        # set up TES autoencoder
        dim = 128
        tes_ae = ae.TESAutoEncoder(grammar, dim)

        # set up training data by randomly sampling trees with at most 3 binary operators
        m = 64
        max_ops = 3
        trees = []
        for i in range(m):
            remaining_ops = max_ops
            root = tree.Tree('$')
            stk = [root]
            while stk:
                par = stk.pop()
                if remaining_ops > 0:
                    if par._label != 'not':
                        r = np.random.choice(5, 1, p = [0.3, 0.3, 0.1, 0.15, 0.15])
                    else:
                        r = np.random.choice(5, 1, p = [0.3, 0.3, 0.0, 0.2, 0.2])
                else:
                    if par._label != 'not':
                        r = np.random.choice(5, 1, p = [0., 0., 0.2, 0.4, 0.4])
                    else:
                        r = np.random.choice(5, 1, p = [0., 0., 0., 0.5, 0.5])
                if r == 0:
                    child = tree.Tree('and')
                    stk.append(child)
                    stk.append(child)
                    remaining_ops -= 1
                elif r == 1:
                    child = tree.Tree('or')
                    stk.append(child)
                    stk.append(child)
                    remaining_ops -= 1
                elif r == 2:
                    child = tree.Tree('not')
                    stk.append(child)
                elif r == 3:
                    child = tree.Tree('x')
                elif r == 4:
                    child = tree.Tree('y')
                else:
                    raise ValueError('internal error!')
                par._children.append(child)
            trees.append(root._children[0].to_list_format())


        # fit the auto-encoder to the data
        tes_ae.fit_svm(trees)

        # check whether the auto-encoding works
        for i in range(len(trees)):
            nodes, adj, _ = tes_ae.decode(tes_ae.encode(*trees[i])[1], max_size = 15)
            self.assertEqual(trees[i][0], nodes)
            self.assertEqual(trees[i][1], adj)


    def test_fit3(self):
        # set up a tree grammar for Boolean formulae in CNF
        alphabet = {'and' : 1, 'or' : 1, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['F', 'C', 'L']
        start = 'F'
        rules = { 'F' : [('and', ['C*'])], 'C' : [('or', ['L*'])], 'L' : [('not', ['L']), ('x', []), ('y', [])] }
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

        # set up TES autoencoder
        dim = 256
        tes_ae = ae.TESAutoEncoder(grammar, dim)

        # set up training data by only sampling non-redundant formulae
        # in pre-defined order
        m = 128
        trees = []
        codings = []
        for i in range(m):
            # there are eight possible clauses with 2 variables, so our entire
            # dataset can be classified by an eight-bit coding. We sample that
            # coding randomly
            coding = np.round(np.random.rand(8))
            codings.append(coding)
            seq = [0]
            if coding[0] > 0.5:
                # generate an or(x) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(1) # generate x
                seq.append(0) # close L*
            if coding[1] > 0.5:
                # generate an or(y) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(2) # generate y
                seq.append(0) # close L*
            if coding[2] > 0.5:
                # generate an or(not(x)) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(0) # generate not(L)
                seq.append(1) # generate x
                seq.append(0) # close L*
            if coding[3] > 0.5:
                # generate an or(not(x)) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(0) # generate not(L)
                seq.append(2) # generate y
                seq.append(0) # close L*
            if coding[4] > 0.5:
                # generate an or(x, y) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(1) # generate x
                seq.append(1) # generate L
                seq.append(2) # generate y
                seq.append(0) # close L*
            if coding[5] > 0.5:
                # generate an or(x, y) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(1) # generate x
                seq.append(1) # generate L
                seq.append(0) # generate not(L)
                seq.append(2) # generate y
                seq.append(0) # close L*
            if coding[6] > 0.5:
                # generate an or(x, y) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(0) # generate not(L)
                seq.append(1) # generate x
                seq.append(1) # generate L
                seq.append(2) # generate y
                seq.append(0) # close L*
            if coding[7] > 0.5:
                # generate an or(x, y) clause
                seq.append(1) # generate C
                seq.append(0) # generate or(L*)
                seq.append(1) # generate L
                seq.append(0) # generate not(L)
                seq.append(1) # generate x
                seq.append(1) # generate L
                seq.append(0) # generate not(L)
                seq.append(2) # generate y
                seq.append(0) # close L*
            seq.append(0) # close C*
            nodes, adj = grammar.produce(seq)
            trees.append((nodes, adj))

        # in a first step, check if we can predict the ground truth coding
        # from the tree coding via kernel regression
        model = KernelRidge(kernel = 'rbf', gamma = 1.)
        tree_codes = []
        # check what the training data looks like
        for i in range(len(trees)):
            seq, h = tes_ae.encode(*trees[i])
            tree_codes.append(h)
        X = np.stack(tree_codes)
        Y = np.stack(codings)
        model.fit(X, Y)

        self.assertTrue(model.score(X, Y) > 0.5)

        # fit the auto-encoder to the data
        tes_ae.fit_svm(trees)

        # check whether the auto-encoding works for most data
        rmse = 0.
        for i in range(len(trees)):
            # auto-encode the ith tree
            nodes, adj, _ = tes_ae.decode(tes_ae.encode(*trees[i])[1], max_size = 2 * len(trees[i][0]) + 5)
            # compute the tree edit distance between the original tree
            # and its auto-encoding
            d = ted.ted(nodes, adj, *trees[i])
            rmse += d * d
        rmse = np.sqrt(rmse / len(trees))
        self.assertTrue(rmse < 3., 'RMSE was unexpectedly high; expected value smaller 3 but got %g' % rmse)

if __name__ == '__main__':
    unittest.main()
