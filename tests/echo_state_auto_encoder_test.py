"""
Implements tests for echo state auto encoders.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import unittest
import numpy as np
from scipy.sparse import csr_matrix
import edist.tree_utils as tree_utils
import edist.ted as ted
import tesae.crj as crj
import tesae.tree as tree
import tesae.tree_grammar as tree_grammar
import tesae.echo_state_auto_encoder as ae

# helper class
class LinearPredictor:

    def __init__(self, w):
        self.w_ = w

    def predict(self, X):
        y = np.dot(X, self.w_)
        return y > 1E-3

class TestESAutoEncoder(unittest.TestCase):

    def test_one_hot_coding(self):
        seq = [0, 3, 1, 2]
        expected_X = np.array([
            [1., 0., 0., 0.],
            [0., 0., 0., 1.],
            [0., 1., 0., 0.],
            [0., 0., 1., 0.]
        ])

        actual_X = ae.one_hot_coding(seq)

        np.testing.assert_array_almost_equal(expected_X, actual_X)

    def test_encode(self):
        T = 10
        n = 3
        dim = 20

        X = np.random.randn(T, n)

        model = ae.ESAutoEncoder(dim)
        model.U_ = crj.setup_input_weight_matrix(n, model._dim, model._v)
        model.W_ = crj.setup_reservoir_matrix(model._dim, model._w_c, model._w_j, model._l)

        h = model.encode(X)
        self.assertEqual(dim, len(h))

    def test_decode_forced(self):
        T = 10
        n = 3
        dim = 20

        X = np.random.randn(T, n)

        model = ae.ESAutoEncoder(dim)
        model.U_ = crj.setup_input_weight_matrix(n, model._dim, model._v)
        model.W_ = crj.setup_reservoir_matrix(model._dim, model._w_c, model._w_j, model._l)

        H = model._decode_forced(np.random.randn(dim), X)

        self.assertEqual(T+1, H.shape[0])
        self.assertEqual(dim, H.shape[1])

    def test_decode(self):
        # set up a reservoir and a coding vector manually in a very
        # specific way with a predictable outcome. In particular, our
        # coding vector stores a 1 in the first dimension which should
        # be remembered for tau steps whereupon the output sequence
        # should stop.
        tau = 10

        model = ae.ESAutoEncoder(tau)
        # the input matrix copies the input into the first neuron
        model.U_ = np.zeros((tau, 1))
        model.U_[0, 0] = 1.
        # the reservoir matrix copies the current neuron into the
        # next neuron until its forgotten
        data = np.ones(tau-1) / np.tanh(1.)
        rows = np.arange(1, tau)
        cols = np.arange(tau-1)
        model.W_ = csr_matrix((data, (rows, cols)), shape=(tau, tau))
        # the output copies the second-to-last neurons value
        model.V_ = np.zeros((1, tau))
        model.V_[0, -2] = 1. / np.tanh(1.)
        # the controller is a function that returns 1 if the last memory
        # cell is 1.
        w = np.zeros(tau)
        w[-1] = 1.
        model.controller_ = LinearPredictor(w)

        h = np.zeros(tau)
        h[0] = 1.
        actual_Y = model.decode(np.tanh(h))

        expected_Y = np.zeros((tau-1, 1))
        expected_Y[-1, 0] = 1.
        np.testing.assert_array_almost_equal(expected_Y, actual_Y, decimal = 3)


    def test_fit(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)


        # set up training data
        trees = [
            (['x'], [[]]),
            (['and', 'x', 'x'], [[1, 2], [], []]),
            (['and', 'and', 'x', 'x', 'x'], [[1, 4], [2, 3], [], [], []]),
            (['and', 'and', 'and', 'x', 'x', 'x', 'x'], [[1, 6], [2, 5], [3, 4], [], [], [], []])
        ]
        parser = tree_grammar.TreeParser(grammar)
        Xs = []
        for i in range(len(trees)):
            Xs.append(ae.one_hot_coding(parser.parse(*trees[i])))

        # set up ES autoencoder
        dim = 64
        model = ae.ESAutoEncoder(dim, one_hot_correction = True)
        # fit the auto-encoder to the data
        model.fit(Xs)

        # check whether the auto-encoding works
        for i in range(len(trees)):
            h = model.encode(Xs[i])
            Y = model.decode(h, max_len = 15)

            _, nodes, adj, valid = ae.produce(grammar, Y)
            self.assertTrue(valid)
            self.assertEqual(trees[i][0], nodes)
            self.assertEqual(trees[i][1], adj)


    def test_fit2(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

        # set up training data by randomly sampling trees with at most 3 binary operators
        m = 64
        max_ops = 3
        trees = []
        for i in range(m):
            remaining_ops = max_ops
            root = tree.Tree('$')
            stk = [root]
            while stk:
                par = stk.pop()
                if remaining_ops > 0:
                    if par._label != 'not':
                        r = np.random.choice(5, 1, p = [0.3, 0.3, 0.1, 0.15, 0.15])
                    else:
                        r = np.random.choice(5, 1, p = [0.3, 0.3, 0.0, 0.2, 0.2])
                else:
                    if par._label != 'not':
                        r = np.random.choice(5, 1, p = [0., 0., 0.2, 0.4, 0.4])
                    else:
                        r = np.random.choice(5, 1, p = [0., 0., 0., 0.5, 0.5])
                if r == 0:
                    child = tree.Tree('and')
                    stk.append(child)
                    stk.append(child)
                    remaining_ops -= 1
                elif r == 1:
                    child = tree.Tree('or')
                    stk.append(child)
                    stk.append(child)
                    remaining_ops -= 1
                elif r == 2:
                    child = tree.Tree('not')
                    stk.append(child)
                elif r == 3:
                    child = tree.Tree('x')
                elif r == 4:
                    child = tree.Tree('y')
                else:
                    raise ValueError('internal error!')
                par._children.append(child)
            trees.append(root._children[0].to_list_format())

        # set up ES autoencoder
        dim = 256
        model = ae.ESTreeWrapper(grammar, dim, w_c = 0.95)

        # fit the auto-encoder to the data
        model.fit(trees)

        # check whether the auto-encoding works at least to some extent
        rmse = 0
        for i in range(len(trees)):
            seq, h = model.encode(*trees[i])
            nodes, adj, _, valid = model.decode(h, max_size = 15)
            d = ted.ted(*trees[i], nodes, adj)
            rmse += d * d
        rmse = np.sqrt(rmse / len(trees))
        self.assertTrue(rmse < 3., 'RMSE was unexpectedly high: %g' % rmse)

if __name__ == '__main__':
    unittest.main()
