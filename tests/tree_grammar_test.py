"""
Implements tests for tree grammars.

Copyright (C) 2020
Benjamin Paaßen
The University of Sydney

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import unittest
import edist.tree_utils as tree_utils
import tesae.tree_grammar as tree_grammar

class TestTreeGrammar(unittest.TestCase):

    def test_check_rule_determinism(self):
        # check a few valid sequences
        tree_grammar.check_rule_determinism([])
        tree_grammar.check_rule_determinism(['A'])
        tree_grammar.check_rule_determinism([3, 'A', 'B'])
        tree_grammar.check_rule_determinism([3, 'A', 'B*', 'B'])
        tree_grammar.check_rule_determinism([3, 'A?', 'A', 'A*'])
        tree_grammar.check_rule_determinism([3, 'A?', 'A', 'A*', 'B*'])
        # now, check a few invalid ones
        with self.assertRaises(ValueError):
            tree_grammar.check_rule_determinism(['A*', 'A*'])
        with self.assertRaises(ValueError):
            tree_grammar.check_rule_determinism(['A?', 'A*'])
        with self.assertRaises(ValueError):
            tree_grammar.check_rule_determinism(['A?', 'B?', 'A*'])

    def test_rule_matches(self):
        # check a few rules
        self.assertEqual([], tree_grammar.rule_matches([], []))
        self.assertIsNone(tree_grammar.rule_matches([], ['A']))
        self.assertEqual(['A'], tree_grammar.rule_matches(['A'], ['A']))
        self.assertEqual([[]], tree_grammar.rule_matches(['A*'], []))
        self.assertEqual([['A']], tree_grammar.rule_matches(['A*'], ['A']))
        self.assertEqual([['A', 'A']], tree_grammar.rule_matches(['A*'], ['A', 'A']))
        self.assertIsNone(tree_grammar.rule_matches(['A*'], ['A', 'B']))
        self.assertIsNone(tree_grammar.rule_matches(['A*', 'A'], []))
        self.assertEqual([[], 'A'], tree_grammar.rule_matches(['A*', 'A'], ['A']))
        self.assertEqual([['A'], 'A'], tree_grammar.rule_matches(['A*', 'A'], ['A', 'A']))
        self.assertEqual([['A', 'A'], 'A'], tree_grammar.rule_matches(['A*', 'A'], ['A', 'A', 'A']))
        self.assertEqual([['A'], 'A', 'A'], tree_grammar.rule_matches(['A*', 'A', 'A'], ['A', 'A', 'A']))
        self.assertIsNone(tree_grammar.rule_matches(['A', 'A*'], []))
        self.assertEqual(['A', []], tree_grammar.rule_matches(['A', 'A*'], ['A']))
        self.assertEqual(['A', ['A']], tree_grammar.rule_matches(['A', 'A*'], ['A', 'A']))
        self.assertEqual([[], [], 'C'], tree_grammar.rule_matches(['A*', 'B?', 'C'], ['C']))
        self.assertEqual([['A'], [], 'C'], tree_grammar.rule_matches(['A*', 'B?', 'C'], ['A', 'C']))
        self.assertEqual([['A', 'A'], [], 'C'], tree_grammar.rule_matches(['A*', 'B?', 'C'], ['A', 'A', 'C']))
        self.assertEqual([[], ['B'], 'C'], tree_grammar.rule_matches(['A*', 'B?', 'C'], ['B', 'C']))
        self.assertEqual([['A'], ['B'], 'C'], tree_grammar.rule_matches(['A*', 'B?', 'C'], ['A', 'B', 'C']))
        self.assertIsNone(tree_grammar.rule_matches(['A*', 'B?', 'C'], ['B', 'B', 'C']))

    def test_rules_intersect(self):
        # check a few non-intersecting rules
        self.assertIsNone(tree_grammar.rules_intersect([], ['A']))
        self.assertIsNone(tree_grammar.rules_intersect(['A'], []))
        self.assertIsNone(tree_grammar.rules_intersect(['A*'], ['B']))
        self.assertIsNone(tree_grammar.rules_intersect(['B'], ['A*']))
        self.assertIsNone(tree_grammar.rules_intersect(['A*'], ['A', 'B']))
        self.assertIsNone(tree_grammar.rules_intersect(['A', 'B'], ['A*']))
        # then, check ya few intersecting rules
        self.assertEqual([], tree_grammar.rules_intersect([], []))
        self.assertEqual([], tree_grammar.rules_intersect([], ['A*']))
        self.assertEqual([], tree_grammar.rules_intersect(['A*'], []))
        self.assertEqual(['A'], tree_grammar.rules_intersect(['A'], ['A*']))
        self.assertEqual(['A'], tree_grammar.rules_intersect(['A*'], ['A']))
        self.assertEqual(['A'], tree_grammar.rules_intersect(['B?', 'A'], ['A*', 'D?']))
        self.assertEqual(['A'], tree_grammar.rules_intersect(['A*', 'D?'], ['B?', 'A']))

    def test_produce(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # produce a simple tree
        nodes_expected = ['and', 'x', 'not', 'x']
        adj_expected   = [[1, 2], [], [3], []]

        nodes_actual, adj_actual = grammar.produce([0, 3, 2, 3])
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)

        # produce a larger tree
        nodes_expected = ['and', 'and', 'x', 'x', 'x']
        adj_expected   = [[1, 4], [2, 3], [], [], []]

        nodes_actual, adj_actual = grammar.produce([0, 0, 3, 3, 3])
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)

    def test_produce2(self):
        # set up a tree grammar for Boolean formulae in CNF
        alphabet = {'and' : 1, 'or' : 1, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['F', 'C', 'L']
        start = 'F'
        rules = { 'F' : [('and', ['C*'])], 'C' : [('or', ['L*'])], 'L' : [('not', ['L']), ('x', []), ('y', [])] }
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # produce a simple tree
        nodes_expected = ['and', 'or', 'x', 'or', 'not', 'x']
        adj_expected   = [[1, 3], [2], [], [4], [5], []]

        nodes_actual, adj_actual = grammar.produce([0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0])
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)

        # produce a larger tree
        nodes_expected = ['and', 'or', 'x', 'y', 'or', 'not', 'not', 'x', 'not', 'y']
        adj_expected   = [[1, 4], [2, 3], [], [], [5, 8], [6], [7], [], [9], []]

        nodes_actual, adj_actual = grammar.produce([0, 1, 0, 1, 1, 1, 2, 0, 1, 0, 1, 0, 0, 1, 1, 0, 2, 0, 0])
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)

    def test_accepts(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # retrieve the tree automaton
        parser = tree_grammar.TreeParser(grammar)
        # set up an example tree which should not be valid because a symbol is unknown
        nodes = ['z']
        adj   = [[]]
        self.assertFalse(parser.accepts(nodes, adj))
        # set up an example tree which should not be valid because the number of children is incorrect
        nodes = ['x', 'x']
        adj   = [[1], []]
        self.assertFalse(parser.accepts(nodes, adj))
        # test a few trees which should be valid
        nodes = ['not', 'x']
        adj   = [[1], []]
        self.assertTrue(parser.accepts(nodes, adj))
        nodes = ['and', 'x', 'y']
        adj   = [[1, 2], [], []]
        self.assertTrue(parser.accepts(nodes, adj))
        nodes = ['and', 'x', 'not', 'x']
        adj   = [[1, 2], [], [3], []]
        self.assertTrue(parser.accepts(nodes, adj))

    def test_parse(self):
        # set up a tree grammar for Boolean formulae
        alphabet = {'and' : 2, 'or' : 2, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['S']
        start = 'S'
        rules = { 'S' : [('and', ['S', 'S']), ('or', ['S', 'S']), ('not', ['S']), ('x', []), ('y', [])]}
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # retrieve the tree automaton
        parser = tree_grammar.TreeParser(grammar)
        # test a few trees which should be valid
        nodes = ['not', 'x']
        adj   = [[1], []]
        expected_seq = [2, 3]
        actual_seq = parser.parse(nodes, adj)
        self.assertEqual(expected_seq, actual_seq)
        nodes = ['and', 'x', 'y']
        adj   = [[1, 2], [], []]
        expected_seq = [0, 3, 4]
        actual_seq = parser.parse(nodes, adj)
        self.assertEqual(expected_seq, actual_seq)
        nodes = ['and', 'x', 'not', 'x']
        adj   = [[1, 2], [], [3], []]
        expected_seq = [0, 3, 2, 3]
        actual_seq = parser.parse(nodes, adj)
        self.assertEqual(expected_seq, actual_seq)

    def test_parse2(self):
        # set up a tree grammar for Boolean formulae in CNF
        alphabet = {'and' : 1, 'or' : 1, 'not' : 1, 'x' : 0, 'y' : 0}
        nonts = ['F', 'C', 'L']
        start = 'F'
        rules = { 'F' : [('and', ['C*'])], 'C' : [('or', ['L*'])], 'L' : [('not', ['L']), ('x', []), ('y', [])] }
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)
        # set up parser
        parser  = tree_grammar.TreeParser(grammar)

        # set up some example trees and check whether they are parsed correctly
        expected_nodes = ['and', 'or', 'x', 'or', 'not', 'x']
        expected_adj   = [[1, 3], [2], [], [4], [5], []]
        expected_seq   = [0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0]
        actual_seq     = parser.parse(expected_nodes, expected_adj)
        self.assertEqual(expected_seq, actual_seq)
        actual_nodes, actual_adj = grammar.produce(actual_seq)
        self.assertEqual(expected_nodes, actual_nodes)
        self.assertEqual(expected_adj, actual_adj)


        nodes_expected = ['and', 'or', 'x', 'y', 'or', 'not', 'not', 'x', 'not', 'y']
        adj_expected   = [[1, 4], [2, 3], [], [], [5, 8], [6], [7], [], [9], []]

        expected_nodes = ['and', 'or', 'x', 'y', 'or', 'not', 'not', 'x', 'not', 'y']
        expected_adj   = [[1, 4], [2, 3], [], [], [5, 8], [6], [7], [], [9], []]
        expected_seq   = [0, 1, 0, 1, 1, 1, 2, 0, 1, 0, 1, 0, 0, 1, 1, 0, 2, 0, 0]
        actual_seq     = parser.parse(expected_nodes, expected_adj)
        self.assertEqual(expected_seq, actual_seq)
        actual_nodes, actual_adj = grammar.produce(actual_seq)
        self.assertEqual(expected_nodes, actual_nodes)
        self.assertEqual(expected_adj, actual_adj)

        # check some malformed elements and check whether the error is correct
        with self.assertRaises(ValueError) as context:
            nodes = ['x']
            adj   = [[]]
            parser.parse(nodes, adj)
        self.assertTrue('starting symbol' in str(context.exception))
        with self.assertRaises(ValueError) as context:
            nodes = ['xor', 'x', 'false']
            adj   = [[1, 2], [], []]
            parser.parse(nodes, adj)
        self.assertTrue('not part of the alphabet' in str(context.exception))
        with self.assertRaises(ValueError) as context:
            nodes = ['and', 'x', 'y']
            adj   = [[1, 2], [], []]
            parser.parse(nodes, adj)
        self.assertTrue('No rule' in str(context.exception))

    def test_parse3(self):
        # set up a grammar with a single optional rule
        alphabet = {'a' : 2, 'b' : 0}
        nonts = ['A', 'B']
        start = 'A'
        rules = { 'A' : [('a', ['B?', 'B'])], 'B' : [('b', [])] }
        grammar = tree_grammar.TreeGrammar(alphabet, nonts, start, rules)

        # set up a simple tree
        seq_expected = [0, 0, 0]
        nodes_expected = ['a', 'b']
        adj_expected = [[1], []]

        nodes_actual, adj_actual = grammar.produce(seq_expected)
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)

        parser  = tree_grammar.TreeParser(grammar)
        seq_actual = parser.parse(nodes_expected, adj_expected)
        self.assertEqual(seq_expected, seq_actual)

        # set up another simple tree
        seq_expected = [0, 1, 0, 0]
        nodes_expected = ['a', 'b', 'b']
        adj_expected = [[1, 2], [], []]

        nodes_actual, adj_actual = grammar.produce(seq_expected)
        self.assertEqual(nodes_expected, nodes_actual)
        self.assertEqual(adj_expected, adj_actual)

        parser  = tree_grammar.TreeParser(grammar)
        seq_actual = parser.parse(nodes_expected, adj_expected)
        self.assertEqual(seq_expected, seq_actual)


if __name__ == '__main__':
    unittest.main()
